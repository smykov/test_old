<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Gift;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gifts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gift-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('I want Gift', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php if (Yii::$app->session->hasFlash('flashMsg')){ ?>
        <div class="alert alert-success">
            <!-- flash message -->
            <?php echo Yii::$app->session->getFlash('flashMsg'); ?>
        </div>
    <?php } ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'value',

            [
                'format' => 'text',
                'content'=>function($data){
                    $buttonChange = '';
                    if ($data->type->name == 'money' && $data->status != Gift::STATUS_PAID)
                        $buttonChange = Html::a('Change to bonus', ['change', 'id' => $data->id ], ['class' => 'btn btn-success']);
                    return $buttonChange . Html::a('Reject', ['delete', 'id' => $data->id ], ['class' => 'btn btn-warning']);
                }
            ],
        ],
    ]); ?>
</div>
