<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GiftVariety */

$this->title = 'Create Gift Variety';
$this->params['breadcrumbs'][] = ['label' => 'Gift Varieties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gift-variety-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
