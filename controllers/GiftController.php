<?php

namespace app\controllers;

use app\models\GiftType;
use app\models\GiftVariety;
use app\models\User;
use Yii;
use app\models\Gift;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * GiftController implements the CRUD actions for Gift model.
 */
class GiftController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->username == 'admin';
                        },
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'create', 'change', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Gift models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Gift::find()->where([
                'user_id' => Yii::$app->user->id,
            ]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Gift model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gift();
        $types = GiftType::find()->with('giftVarieties')->all();
        do {
            $type = $types[array_rand($types)];
        } while (count($type->giftVarieties) == 0);

        $viariety = $type->giftVarieties[array_rand($type->giftVarieties)];

        $model->value = (string)rand($type->min, $type->max);
        $model->name = $viariety->name  ;
        $model->type_id = $type->id;
        $model->user_id = Yii::$app->user->id;

        if(!$model->save()){
            Yii::$app->session->setFlash('flashMsg', Yii::t('common', "ERROR \nPlease, try again."));
            return $this->redirect(['index']);
        }

        $viariety->amount -= $model->value;
        $viariety->save('amount');

        if($viariety->name == 'bonus'){
            User::findOne(['id' => Yii::$app->user->id])->addBonus($model->value);
        }

        Yii::$app->session->setFlash('flashMsg', Yii::t('common', 'Your win {gift}', ['gift' => $type->name]));
        return $this->redirect(['index']);
    }

    /**
     * Change to bonus.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionChange($id)
    {
        $model = $this->findModel($id);
        $bonus = GiftVariety::findOne(['name' => 'bonus']);
        $oldGift = $model->name;

        if (!($bonus instanceof GiftVariety )){
            Yii::$app->session->setFlash('flashMsg', Yii::t('common', "ERROR \nPlease, try again."));
            return $this->redirect(['index']);
        }

        $model->type_id = $bonus->type_id;
        $model->name = $bonus->name;
        $model->value = (string)($model->value * 5);

        if (!$model->save()){
            Yii::$app->session->setFlash('flashMsg', Yii::t('common', "ERROR \nPlease, try again."));
            return $this->redirect(['index']);
        }

        User::findOne(['id' => Yii::$app->user->id])->addBonus($model->value);
        Yii::$app->session->setFlash('flashMsg', Yii::t('common', 'Your change {gift} to bonus', ['gift' => $oldGift]));
        return $this->redirect(['index']);

    }

    /**
     * Deletes an existing GiftVariety model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Gift model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gift the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gift::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
