<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 23.01.20
 * Time: 18:21
 */

namespace app\services\Pay;

use app\models\Gift;

/**
 * Class PayService
 * @package app\services\Pay
 */
class PayService
{

    /**
     * @var PayServiceInterface
     */
    private $pay;

    /**
     * PayService constructor.
     *
     * @param PayServiceInterface pay
     */
    public function __construct(PayServiceInterface $pay)
    {
        $this->pay = $pay;
    }

    /**
     * @param Gift $gift
     * @return mixed
     */
    public function send(Gift $gift)
    {
        return $this->send($gift);
    }

}