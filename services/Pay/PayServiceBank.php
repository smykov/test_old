<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 23.01.20
 * Time: 16:22
 */

namespace app\services\Pay;

use app\models\Gift;
use linslin\yii2\curl\Curl;


/**
 * Class PayServiceBank
 * @package app\services\Pay
 */
class PayServiceBank implements PayServiceInterface
{
    /**
     * api url for bank
     */
    const URL = 'http://bank.com/';

    /**
     * @param Gift $gift
     * @return bool
     */
    public function send(Gift $gift)
    {

        $curl = new Curl();

        $curl->get(self::URL);

        if ($curl->responseCode == 200) {
            return true;
        }

        return false;
    }
}