<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 23.01.20
 * Time: 18:34
 */

namespace app\services\Pay;

use app\models\Gift;

/**
 * Interface PayServiceInterface
 * @package app\services\Pay
 */
interface PayServiceInterface
{
    /**
     * @param Gift $gift
     * @return mixed
     */
    public function send(Gift $gift);
}