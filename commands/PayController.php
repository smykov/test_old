<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 29.11.18
 * Time: 17:38
 */

namespace app\commands;


use app\models\Gift;
use app\services\Pay\PayServiceBank;
use app\services\Pay\PayService;
use yii\console\ExitCode;
use app\repositories\Gift\GiftRepository;

class PayController extends \yii\console\Controller
{

    public function actionIndex($limit = 10)
    {
        $pay = new PayService(new PayServiceBank());
        $rGift = new GiftRepository();

        $gifts = $rGift->getNewMoneyGifts($limit);

        foreach ($gifts as $gift) {
            /** @var Gift $gift */
            if ($pay->send($gift)) {
                $rGift->confirmPayment($gift);
            }
        }

        return ExitCode::OK;
    }

}