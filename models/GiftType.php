<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gift_type".
 *
 * @property int $id
 * @property string $name
 * @property int $min
 * @property int $max
 *
 * @property Gift[] $gifts
 * @property GiftVariety[] $giftVarieties
 */
class GiftType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gift_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'min', 'max'], 'required'],
            [['min', 'max'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'min' => 'Min',
            'max' => 'Max',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGifts()
    {
        return $this->hasMany(Gift::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGiftVarieties()
    {
        return $this->hasMany(GiftVariety::className(), ['type_id' => 'id'])
            ->andOnCondition(['>', 'amount', 0]);
    }
}
