<?php

namespace app\models;

/**
 * This is the model class for table "gift".
 *
 * @property int $id
 * @property string $value
 * @property string $name
 * @property int $status
 * @property int $type_id
 * @property int $user_id
 *
 * @property GiftType $type
 * @property User $user
 */
class Gift extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_PAID = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gift';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value', 'name', 'type_id', 'user_id'], 'required'],
            [['type_id', 'user_id', 'status'], 'integer'],
            [['value', 'name'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => GiftType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'name' => 'Name',
            'status' => 'Status',
            'type_id' => 'Type',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(GiftType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


}