<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 23.01.20
 * Time: 16:36
 */

namespace app\repositories\Gift;

use app\models\Gift;
use app\models\GiftType;

/**
 * Class GiftRepository
 * @package app\services\Gift
 */
class GiftRepository implements GiftRepositoryInterface
{
    /**
     * @param Gift $gift
     * @return bool
     */
    public function confirmPayment(Gift $gift)
    {

        $gift->status = Gift::STATUS_PAID;

        return $gift->save();

    }

    /**
     * @param int $limit
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getNewMoneyGifts(int $limit)
    {

        return Gift::find()->where(['status' => Gift::STATUS_NEW, GiftType::tableName() . '.name' => 'money'])->joinWith('type')->limit($limit)->all();
    }
}