<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 24.01.20
 * Time: 14:19
 */

namespace app\repositories\Gift;

use app\models\Gift;

/**
 * Interface GiftRepositoryInterface
 * @package app\repositories\Gift
 */
interface GiftRepositoryInterface
{
    /**
     * @param Gift $gift
     * @return mixed
     */
    public function confirmPayment(Gift $gift);

    /**
     * @param int $limit
     * @return mixed
     */
    public function getNewMoneyGifts(int $limit);

}