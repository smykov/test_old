<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m181127_081716_user
 */
class m181127_081716_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('user', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'password' => Schema::TYPE_STRING . ' NOT NULL',
            'bonus' => Schema::TYPE_INTEGER . ' DEFAULT 0',
            'authKey' => Schema::TYPE_STRING . ' NOT NULL',
            'accessToken' => Schema::TYPE_STRING . ' NOT NULL'
        ]);
        $this->insert('user', [
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token'
        ]);
        $this->insert('user', [
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token'
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181127_081716_user cannot be reverted.\n";
        $this->dropTable('user');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181127_081716_user cannot be reverted.\n";

        return false;
    }
    */
}
