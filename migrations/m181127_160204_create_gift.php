<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m181127_160204_create_gift
 */
class m181127_160204_create_gift extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('gift', [
            'id' => Schema::TYPE_PK,
            'value' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'status' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
            'type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL'
        ]);
        $this->addForeignKey('gift_user', 'gift', 'user_id', 'user', 'id','RESTRICT', 'RESTRICT');
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181127_160204_create_gift cannot be reverted.\n";
        $this->dropTable('gift');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181127_160204_create_gift cannot be reverted.\n";

        return false;
    }
    */
}
