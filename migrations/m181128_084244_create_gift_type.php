<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m181128_084244_create_gift_type
 */
class m181128_084244_create_gift_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('gift_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'min' => Schema::TYPE_INTEGER . ' NOT NULL',
            'max' => Schema::TYPE_INTEGER . ' NOT NULL'
        ]);
        $this->addForeignKey('gift_type_gift', 'gift', 'type_id', 'gift_type', 'id','CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181128_084244_create_gift_type cannot be reverted.\n";
        $this->dropForeignKey('gift_type_gift','gift');
        $this->dropTable('gift_type');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181128_084244_create_gift_type cannot be reverted.\n";

        return false;
    }
    */
}
