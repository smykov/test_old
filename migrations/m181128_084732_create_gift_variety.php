<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m181128_084732_create_gift_variety
 */
class m181128_084732_create_gift_variety extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('gift_variety', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'amount' => Schema::TYPE_INTEGER . ' DEFAULT 0',
            'type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->addForeignKey('gift_type_gift_variety','gift_variety', 'type_id','gift_type','id','CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181128_084732_create_gift_variety cannot be reverted.\n";
        $this->dropTable('gift_variety');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181128_084732_create_gift_variety cannot be reverted.\n";

        return false;
    }
    */
}
