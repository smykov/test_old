<?php

use yii\db\Migration;
use app\models\GiftType;
use app\models\GiftVariety;

/**
 * Class m181128_110741_add_data_for_gifts
 */
class m181128_110741_add_data_for_gifts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $gift[0] = new GiftType([
            'name' => 'money',
            'min' => 1,
            'max' => 10
        ]);
        $gift[0]->save();
        $gift[1] = new GiftType([
            'name' => 'bonus',
            'min' => 100,
            'max' => 1000
        ]);
        $gift[1]->save();
        $gift[2] = new GiftType([
            'name' => 'BOX',
            'min' => 1,
            'max' => 1
        ]);
        $gift[2]->save();
        (new GiftVariety([
            'name' => 'USD',
            'amount' => 10000,
            'type_id' => $gift[0]->id
        ]))->save();
        (new GiftVariety([
            'name' => 'EUR',
            'amount' => 10000,
            'type_id' => $gift[0]->id
        ]))->save();
        (new GiftVariety([
            'name' => 'bonus',
            'amount' => 999999999,
            'type_id' => $gift[1]->id
        ]))->save();
        (new GiftVariety([
            'name' => 'BOX1',
            'amount' => 100,
            'type_id' => $gift[2]->id
        ]))->save();
        (new GiftVariety([
            'name' => 'BOX2',
            'amount' => 100,
            'type_id' => $gift[2]->id
        ]))->save();
        (new GiftVariety([
            'name' => 'BOX3',
            'amount' => 100,
            'type_id' => $gift[2]->id
        ]))->save();
        (new GiftVariety([
            'name' => 'BOX4',
            'amount' => 100,
            'type_id' => $gift[2]->id
        ]))->save();
        (new GiftVariety([
            'name' => 'BOX5',
            'amount' => 100,
            'type_id' => $gift[2]->id
        ]))->save();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181128_110741_add_data_for_gifts cannot be reverted.\n";
        GiftType::deleteAll();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181128_110741_add_data_for_gifts cannot be reverted.\n";

        return false;
    }
    */
}
